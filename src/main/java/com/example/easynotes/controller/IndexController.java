package com.example.easynotes.controller;


import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexController {
    
    Logger LOG = LoggerFactory.getLogger(IndexController.class);

    @GetMapping
    public Object sayHello() {
        
        Map<String,Object> res = new HashMap();
        res.put("success",true);
        return res;
    }
}
