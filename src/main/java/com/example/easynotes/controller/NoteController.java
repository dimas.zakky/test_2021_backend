package com.example.easynotes.controller;

import com.example.easynotes.model.Customer;
import com.example.easynotes.repository.CustomerRepository;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class NoteController {

    Logger LOG = LoggerFactory.getLogger(IndexController.class);
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @RequestMapping("/ping")
    public Object ping(){
        Map<String,Object> res = new HashMap();
        res.put("success",true);
        res.put("message","Ping from api");
        return res;
    }
    
    
    @PostMapping(value = "/data/save")
    public Object processData(
            String customerData
    ) throws Exception{
        LOG.info("processData. USER INPUT = "+customerData);
        Map<String,Object> res = new HashMap();
        res.put("success",true);
        res.put("customerData",customerData);
        
        Matcher matcher = Pattern.compile("\\d+").matcher(customerData);
        boolean ageResult = matcher.find();
        LOG.info("matcher.matches()====="+ageResult);
        if (!ageResult){
            res.put("success",false);
            res.put("message","WRONG FORMAT. NO AGE DATA");
            return res;
        }
        int age = Integer.valueOf(matcher.group());
        String []rawData = customerData.split(age+"");
        if (rawData.length == 0){
            // berarti hanya input angka saja
            res.put("success",false);
            res.put("message","WRONG FORMAT. NO NAME PROVIDED");
            return res;
        }else if (rawData.length > 0 && rawData[0].length() == 0){
            res.put("success",false);
            res.put("message","WRONG FORMAT. NO NAME PROVIDED");
            return res;
        }
       
        String realname = rawData[0];
        LOG.info("realname lange = "+realname.length());
        res.put("realname",realname.trim());
        res.put("age",age);
        
        
        // bersihkan kemungkinan muncul kata TAHUN, THN dan TH
        if (rawData.length <=1 ){
            res.put("success",false);
            res.put("message","WRONG FORMAT. NO ADDRESS PROVIDED");
            return res;
        }
        String clearedData = rawData[1];
        LOG.info("clearedData==="+clearedData);
        clearedData = clearedData.replaceAll("(?i)TAHUN", "");
        clearedData = clearedData.replaceAll("(?i)THN", "");
        clearedData = clearedData.replaceAll("(?i)TH", "");
        
        String city = clearedData;
        res.put("city",city.trim());
        
        
        Customer c = new Customer(realname, age, city);
        customerRepository.save(c);
        
        return res;
        
    }
}
